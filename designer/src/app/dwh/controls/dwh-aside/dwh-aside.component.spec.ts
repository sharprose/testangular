import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DwhAsideComponent } from './dwh-aside.component';

describe('DwhAsideComponent', () => {
  let component: DwhAsideComponent;
  let fixture: ComponentFixture<DwhAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DwhAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DwhAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
