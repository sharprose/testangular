import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadataHomePageComponent } from './metadata-home-page.component';

describe('MetadataHomePageComponent', () => {
  let component: MetadataHomePageComponent;
  let fixture: ComponentFixture<MetadataHomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetadataHomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetadataHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
