import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadataAsideComponent } from './metadata-aside.component';

describe('MetadataAsideComponent', () => {
  let component: MetadataAsideComponent;
  let fixture: ComponentFixture<MetadataAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetadataAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetadataAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
