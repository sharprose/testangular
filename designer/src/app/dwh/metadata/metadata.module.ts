import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { MaterialModule } from '../../material/material.module';

import { MetadataAsideComponent } from './controls/metadata-aside/metadata-aside.component';

import { MetadataHomePageComponent } from '../../dwh/metadata/pages/metadata-home-page/metadata-home-page.component';
import {HomePageComponent} from "../pages/home-page/home-page.component";

export const metadataRoute : Routes = [
  {
    path: 'Mtd',
    component: MetadataHomePageComponent,
    children: []
  }
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    MetadataAsideComponent,
  ],
  declarations: [MetadataAsideComponent, MetadataHomePageComponent]
})

export class MetadataModule { }
