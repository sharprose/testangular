import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtlAsideComponent } from './etl-aside.component';

describe('EtlAsideComponent', () => {
  let component: EtlAsideComponent;
  let fixture: ComponentFixture<EtlAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtlAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtlAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
