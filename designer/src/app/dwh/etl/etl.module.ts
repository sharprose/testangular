import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material/material.module';
import {Routes} from "@angular/router";

import { EtlAsideComponent } from './controls/etl-aside/etl-aside.component';
import { HomeComponent } from './pages/home/home.component';


export const etlRoute : Routes = [
  {
    path: 'Etl',
    component: HomeComponent,
    children: []
  }
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    EtlAsideComponent
  ],
  declarations: [EtlAsideComponent, HomeComponent]
})

export class EtlModule { }
