import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerAsideComponent } from './scheduler-aside.component';

describe('SchedulerAsideComponent', () => {
  let component: SchedulerAsideComponent;
  let fixture: ComponentFixture<SchedulerAsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulerAsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerAsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
