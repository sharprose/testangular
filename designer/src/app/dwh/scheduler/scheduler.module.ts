import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material/material.module';

import { HomeComponent } from './pages/home/home.component';
import {  } from './controls/aside/scheduler-aside';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    SchedulerAsideComponent
  ],
  declarations: [HomeComponent, SchedulerAsideComponent]
})

export class SchedulerModule { }
