import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { MaterialModule } from '../material/material.module';

import { EtlModule } from './etl/etl.module';
import { MetadataModule, metadataRoute } from './metadata/metadata.module';
import { SchedulerModule } from './scheduler/scheduler.module';

import { DwhAsideComponent } from './controls/dwh-aside/dwh-aside.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

export const routeDwhModule : Routes = [
  {
    path: 'Dwh',
    component: HomePageComponent,
    children: metadataRoute
  }
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,

    EtlModule,
    MetadataModule,
    SchedulerModule
  ],
  exports: [
    DwhAsideComponent
  ],
  declarations: [DwhAsideComponent, HomePageComponent]
})

export class DwhModule { }
