import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule, Routes } from '@angular/router';

import { MaterialModule } from '../material/material.module';

import { NewsComponent } from './pages/news/news.component';
import { HomeComponent } from './pages/home/home.component';

export const routeHomeModule : Routes = [
  {
    path: '',
    redirectTo: 'Home',
    pathMatch: 'full'
  },
  {
    path: 'Home',
    component: HomeComponent,
    children: [
      {
        path: 'news',
        component: NewsComponent
      }
    ]
  },
  {
    path: 'news',
    component: NewsComponent
  }
]

@NgModule({
  imports: [
    FlexLayoutModule,
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
  ],
  declarations: [NewsComponent, HomeComponent]
})
export class HomeModule { }
