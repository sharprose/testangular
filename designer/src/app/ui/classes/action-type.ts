export enum ActionType {
  Page = 0,

  Separator = 1,

  Frame = 2
}
