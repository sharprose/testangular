import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  @Input() AllowHide: boolean = true;
  @Input() Width: string;
  IsHide: boolean;

  constructor() {
    this.IsHide = false;
  }

  ngOnInit() {
  }

  showHide(){
    this.IsHide = !this.IsHide;
  }
}
