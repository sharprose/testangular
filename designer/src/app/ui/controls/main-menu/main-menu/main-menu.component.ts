import { Component, OnInit } from '@angular/core';

import { MainMenuService } from "../services/main-menu.service";

import { MenuModel } from "../models/menu-model";
import { MenuItemModel } from "../models/menu-item-model";
import { ActionType } from "../../../classes/action-type";


@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {
  public Menu: MenuModel;

  constructor( private service: MainMenuService) {
    this.service.Get().subscribe(menu => this.Menu = menu);
  }

  ngOnInit() {
  }
}
