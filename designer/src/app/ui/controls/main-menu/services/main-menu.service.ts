import { Injectable } from '@angular/core';
import {MenuModel} from "../models/menu-model";
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {MenuItemModel} from "../models/menu-item-model";
import {ActionType} from "../../../classes/action-type";

@Injectable()
export class MainMenuService {

  constructor() {}

  Get() : Observable<MenuModel> {
    let menu = new MenuModel();

    let home = new MenuItemModel();
    home.Title = "Home";
    home.Url='/';
    home.Type = ActionType.Page;
    menu.Add(home);

    let news = new MenuItemModel();
    news.Title = "News";
    news.Url='/news';
    news.Type = ActionType.Page;
    menu.Add(news);

    let news2 = new MenuItemModel();
    news2.Title = "News2";
    news2.Url='Home/news';
    news2.Type = ActionType.Page;
    menu.Add(news2);

    let system = new MenuItemModel();
    system.Title = "System";
    system.Url = "system";
    system.Type = ActionType.Page;
    menu.Add(system);

    let separator = new MenuItemModel();
    separator.Type = ActionType.Separator;
    menu.Add(separator);

    let me = new MenuItemModel();
    me.Title = "Me";
    me.Url = "profile";
    me.Type = ActionType.Page;
    menu.Add(me);

    return of(menu);
  }
}
