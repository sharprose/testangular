import { MenuItemModel } from "./menu-item-model";

export class MenuModel {

  public Items: Array<MenuItemModel>;

  constructor(){
    this.Items = new Array<MenuItemModel>();
  }

  Add(item: MenuItemModel){
    this.Items.push(item);
  }
}
