import {ActionType} from "../../../classes/action-type";

export class MenuItemModel {
  Title: string;
  Type: ActionType;
  Url: string;

  constructor(){

  }
}
