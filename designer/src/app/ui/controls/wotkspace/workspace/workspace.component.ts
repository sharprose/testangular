import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {
  showMenu: boolean;

  constructor() {
    this.showMenu = true;
  }

  ngOnInit() {
  }

}
