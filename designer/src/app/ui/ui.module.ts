import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material/material.module';
import { HomeModule, routeHomeModule } from '../home/home.module';

import { KeepHtmlPipe } from "./pipes/keep-html-pipe";

import { MainMenuService } from "./controls/main-menu/services/main-menu.service";

import { MainMenuComponent } from './controls/main-menu/main-menu/main-menu.component';
import { WorkspaceComponent } from './controls/wotkspace/workspace/workspace.component';
import { ContainerComponent } from './controls/container/container.component';
import { AsideComponent } from './controls/aside/aside.component';
import { PageContainerComponent } from './controls/page-container/page-container.component';
import { DwhModule, routeDwhModule } from "../dwh/dwh.module";

export const globalRoute = [...routeDwhModule, ...routeHomeModule];

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    MaterialModule,

    DwhModule
  ],
  exports: [
    MainMenuComponent,
    WorkspaceComponent
  ],
  declarations: [
    KeepHtmlPipe,
    MainMenuComponent,
    WorkspaceComponent,
    PageContainerComponent,
    ContainerComponent,
    AsideComponent
  ],
  providers:[
    MainMenuService
  ]
})

export class UiModule { }
