export class AuthResult {
  Success: boolean;
  Token: string;
  Message: string;
}
