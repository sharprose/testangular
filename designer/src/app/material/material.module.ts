import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatProgressBarModule,
  MatCheckboxModule,
  MatSelectModule,
  MatTooltipModule,
  MatChipsModule,
  MatTableModule,
  MatIconModule,
  MatToolbarModule,
  MatGridListModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
  MatLineModule,
  MatDividerModule,
  MatListModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  imports: [
    BrowserAnimationsModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTooltipModule,
    MatChipsModule,
    MatTableModule,
    MatIconModule,
    MatToolbarModule,
    MatGridListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatLineModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule
  ],
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTooltipModule,
    MatChipsModule,
    MatTableModule,
    MatIconModule,
    MatToolbarModule,
    MatGridListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatLineModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule
  ],
  declarations: []
})
export class MaterialModule { }
