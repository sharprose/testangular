import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';

import { UiModule, globalRoute } from './ui/ui.module';

import { HomeModule } from './home/home.module';
import { DwhModule } from './dwh/dwh.module';

const routeParams = {
  enableTracing: true,
}

@NgModule({
  imports: [
    BrowserModule,

    RouterModule.forRoot(globalRoute, routeParams),

    FlexLayoutModule,
    UiModule,

    HomeModule,
    DwhModule
  ],
  providers: [

  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
